//Собственно невероятно полезная функция которую много где используем - отправка POST и GET запросов через javascript без перезагрузки страницы

//GET-запрос
$(document).ready(function(){
	$("#submit_query").click(function() { //при клике на кнопку <button id="submit_query">Отправить</button> выполняем функцию
		$.get("/actions/get.php?id='.$id.'", //тут указываем URL вместе с GET-запросом (запросов можно составить несколько). Тут в качестве значения id указана переменная из PHP, но можно сделать и без неё
		function(data) {
			$("#result").html(data); //вывод ответа от php-скрипта в <div id="result"></div>
		});
		return false;
	});
});

//POST-запрос
$(document).ready(function(){
	$("#submit_form").click(function(){ //при клике на кнопку <button id="submit_form">Отправить</button> выполняем функцию
		$.post("/actions/post.php", $("#form_id").serialize(),  function(response) { //здесь #form_id - это ID формы, которая будет отправляться
			$("#result").html(response); //как и в GET -  вывод ответа от php-скрипта в <div id="result"></div>
		});
		return false;
	});
});

//Вот и всё. Относительно простые, но крайне полезные функции!